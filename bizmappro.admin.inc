<?php

/**
 * @file
 * Bizmappro settings UI.
 * We configure the following settings in the admin area:
 *  - BMP host: stage or live
 *  - subscription key
 *  - gmap API key
 *  - cache directory (caches JS and CSS files locally)
 */



/**
 * MENU CALLBACK: provides an admin form to configure the bizmap pro settings
 */
function bizmappro_admin_form(&$form_state) {
  $form = array();

  if(!variable_get('bizmappro_is_configured', false)) {
    $form['bizmappro_not_configured'] = array(
      '#type' => 'markup',
      '#value' => t('BizMap Pro has not been configured.'),
      '#prefix' => '<p class="warning">',
      '#suffix' => '</p>',
    );      
  }

  $bmp_hosts = array(
    'http://' . BIZMAPPRO_MAP_HOST_STAGE => t('Stage'),
    'http://' . BIZMAPPRO_MAP_HOST_LIVE  => t('Live'),
  );

  $form['bizmappro_host'] = array(
    '#type' => 'radios',
    '#title' => 'BizMap Pro host',
    '#options' => $bmp_hosts,
    '#default_value' => variable_get('bizmappro_host', 'http://' . BIZMAPPRO_MAP_HOST_STAGE),
  );

  $form['bizmappro_subscription_key'] = array(
    '#type' => 'textfield',
    '#title' => 'Subscription key',
    '#default_value' => variable_get('bizmappro_subscription_key', ''),
  );

  $form['bizmappro_gmap_api_key'] = array(
    '#type' => 'textfield',
    '#title' => 'Gmap API key',
    '#default_value' => variable_get('bizmappro_gmap_api_key', ''),
  );

  $form['bizmappro_cache_dir'] = array(
    '#type' => 'textfield',
    '#title' => 'Cache directory',
    '#description' => 'Remote javascript and css files will be cached locally in this directory.',
    '#default_value' => variable_get('bizmappro_cache_dir', BIZMAPPRO_SUGGESTED_CACHE_DIR),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
  );


  return $form;
}


/**
 * FORM VALIDATOR CALLBACK: 
 * Ensure that the user has provided a valid cache directory.  we won't store data in the root of the files directory!
 */
function bizmappro_admin_form_validate($form, &$form_state) {
  if(empty($form_state['values']['bizmappro_cache_dir'])) {
    form_set_error('bizmappro_cache_dir', 'You must set a cache directory.');
  }
}


/**
 * FORM SUBMIT CALLBACK:
 * Save our config.
 */
function bizmappro_admin_form_submit($form, &$form_state) {
  // if the cache directory has changed, make sure we remove the old cache directory.
  $old_cache_dir = variable_get('bizmappro_cache_dir', '');
  if ($old_cache_dir && $old_cache_dir != $form_state['values']['bizmappro_cache_dir']) {
	$cache_dir_path = file_directory_path() . '/' . $old_cache_dir;
    file_delete($cache_dir_path . '/' . BIZMAPPRO_CACHE_FILE_BMP_SCRIPT);
    file_delete($cache_dir_path . '/' . BIZMAPPRO_CACHE_FILE_BMP_STYLESHEET);
    file_delete($cache_dir_path . '/' . BIZMAPPRO_CACHE_FILE_GMAP_SCRIPT);  
    // attempt to remove the directory - it'll fail if it's not empty (we'll use @ to surpress the warning)
    @rmdir($cache_dir_path);
  }

  variable_set('bizmappro_host',             $form_state['values']['bizmappro_host']);
  variable_set('bizmappro_subscription_key', $form_state['values']['bizmappro_subscription_key']);
  variable_set('bizmappro_gmap_api_key',     $form_state['values']['bizmappro_gmap_api_key']);
  variable_set('bizmappro_cache_dir',        $form_state['values']['bizmappro_cache_dir']);

  // the reset cache function will download the relevant remote files, cache them, and set a variable if successful
  bizmappro_reset_cache();  
}
