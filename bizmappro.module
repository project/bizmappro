<?php

/**
 * @file
 * The core bizmappro module file.  Provides an admin page to configure BMP, and a block to present the map.
 */

// URL for the BizMap Pro company homepage
define('BIZMAPPRO_HOMEPAGE', 'http://www.bizmappro.com/');

// URL to sign up for a Google map API key
define('BIZMAPPRO_GMAP_SIGNUP_URL', 'http://code.google.com/apis/maps/signup.html');

define('BIZMAPPRO_MAP_HOST_LIVE',  'mapcontrol.bizmappro.com');
define('BIZMAPPRO_MAP_HOST_STAGE', 'mapcontrolstaging.bizmappro.com');

// external JS and CSS files are cached locally.  This directory is based on the drupal files directory.
define('BIZMAPPRO_SUGGESTED_CACHE_DIR', 'bizmappro_cache');

// Cached files will use the following filenames:
define('BIZMAPPRO_CACHE_FILE_BMP_SCRIPT',      'BizMapProv3.js');
define('BIZMAPPRO_CACHE_FILE_BMP_STYLESHEET',  'bizmappro.css');
define('BIZMAPPRO_CACHE_FILE_GMAP_SCRIPT',     'gmap.js');

define('BIZMAPPRO_GMAP_STYLESHEET',            'bizmappro_gmap.css');




/**
 * Implementation of hook_help().
 */
function bizmappro_help($path, $arg) {
  switch ($path) {
    case 'admin/help#bizmappro':
      $output = '';
      $output .= '<p>' . t('To use the Bizmap Pro module, you will need an account with !bizmappro_url.', array('!bizmappro_url' => l('BizMap Pro', BIZMAPPRO_HOMEPAGE))) . '</p>';
      $output .= '<blockquote>';
      $output .= '<p>' . t('BizMap Pro is an online search &amp; marketing software solution for any business that needs to promote multiple business locations via a web site.') . '</p>';
      $output .= '<p>' . t('With BizMap Pro you can add a store locator, pub finder, property search or service directory to your web site in minutes and then fully analyse the success of your online marketing activity with the BizMap Pro Analysis Centre.') . '</p>';
      $output .= '</blockquote>';
      $output .= '<p>' . t('You will also need a Google map API key.  !gmap_signup_url.', array('!gmap_signup_url' => l(t('Sign up for a Google map API key'), BIZMAPPRO_GMAP_SIGNUP_URL))) . '</p>';
     return $output;

    case 'admin/settings/bizmappro':
      $output = '<p>' . t('This module caches JavaScript and CSS from BizMap Pro and Google gmaps.   Saving this form will flush the cache and re-fetch the remote files.') . '</p>';
      return $output;
  }
}


/**
 * Implementation of hook_menu()
 */
function bizmappro_menu() {
  $items = array();

  $items['admin/settings/bizmappro'] = array(
    'page_callback' => 'bizmappro_admin_form',
    'title' => 'BizMap Pro',
    'description' => "Set your BizMap Pro subscription key and Gmap API key.",
    'page callback' => 'drupal_get_form',
    'page arguments' => array('bizmappro_admin_form'),
    'access arguments' => array('administer bizmappro'),
    'file' => 'bizmappro.admin.inc',
    );

  return $items;	
}


/**
 * Implementation of hook_perm
 */
function bizmappro_perm() {
  return array('administer bizmappro');
}


/**
 * Implementation of hook_block()
 */
function bizmappro_block($op = 'list', $delta = 0, $edit = array()) {
  if ($op == 'list') {
    return array(
      'map' => array('info' => t('BizMap Pro Map'), 'cache' => 'BLOCK_CACHE_GLOBAL'),
      );
  }
  elseif ($op == 'view') {
    switch ($delta) {
      case 'map':
        return _bizmappro_block_map();
    }
  }
}



/**
 * Block view function to present the BMP map
 */
function _bizmappro_block_map() {
  // Test that bizmappro is properly configured - if not, try to refresh the cache.
  if (variable_get('bizmappro_is_configured', false) || bizmappro_reset_cache()) {
    // The init function loads all the relevant js and css files
    _bizmappro_init_bmp();
    return array('content' => '<div id="BizMap"></div>');
  }
}





/**
 * BMP init function: adds all required assets to the page
 */
function _bizmappro_init_bmp() {
  
  /**
   * We load from local cache:
   *  - BMP main script file
   *  - BMP stylesheet (client-specific: requires BMP Subscription key)
   *  - GMAP script (client-specific: requires GMAP API key)
   */  
  $local_cache_directory = file_directory_path() . '/' . variable_get('bizmappro_cache_dir', '');
  $subscription_key = variable_get('bizmappro_subscription_key', '');


  // add the BMP script
  drupal_add_js($local_cache_directory . '/' . BIZMAPPRO_CACHE_FILE_BMP_SCRIPT, 'module', 'header', false, false, false);
  
  // add the BMP stylesheet
  drupal_add_css($local_cache_directory . '/' . BIZMAPPRO_CACHE_FILE_BMP_STYLESHEET, 'module', 'all', false);

  // add the BMP inline script (calling function) and gmap unload script.
  $inline_script = "
    $(document).ready(function() {
      BizMapLoad('" . $subscription_key . "')
    });
    window.onunload = GUnload;
  ";
  drupal_add_js($inline_script, 'inline');

  // add the gmap style
  $gmap_stylesheet = drupal_get_path('module', 'bizmappro') . '/' . BIZMAPPRO_GMAP_STYLESHEET;
  drupal_add_css($gmap_stylesheet, 'module', 'all', false); 

  // add the gmap script
  drupal_add_js($local_cache_directory . '/' . BIZMAPPRO_CACHE_FILE_GMAP_SCRIPT, 'module', 'header', false, false, false);
}



/**
 * Implementation of hook_flush_caches()
 * hook_flush_caches should return an array of tables to clear.
 * As we cache data in the FS, we hook here to call our custom cache reset function.
 */
function bizmappro_flush_caches() {
  bizmappro_reset_cache();
  return array();
}


/**
 * custom cache reset function
 */
function bizmappro_reset_cache() {
  // test that the module is properly configured before trying to fetch the remote files
  $subscription_key = variable_get('bizmappro_subscription_key', false);
  $gmap_api_key = variable_get('bizmappro_gmap_api_key', false);
  $cache_dir = variable_get('bizmappro_cache_dir', false);
  $bmp_host = variable_get('bizmappro_host', false);

  if (
    empty($subscription_key) ||
    empty($gmap_api_key) ||
    empty($cache_dir) ||
    empty($bmp_host)
  ) {
    drupal_set_message(t('BizMap Pro has not been properly configured.  Set your subscription key and gmap API key at the !configuration_page.', array('!configuration_page' => l('configuration page', 'admin/settings/bizmappro'))), 'error');
    variable_del('bizmappro_is_configured');
    return false;
  }

  $cache_dir_path = file_directory_path() . '/' . $cache_dir;

  // clear old cached data from the cache directory; get new cached data.
  if (!file_check_directory($cache_dir_path, true)) {
    // cache directory is invalid and can't be created / made writable
    drupal_set_message(t('BizMap Pro cannot use cache directory %cache_directory.<br /> Check your file permissions, or change the cache directory at the !configuration_page.', array('%cache_directory' => $cache_dir_path, '!configuration_page' => l('admin/settings/bizmappro', 'configuration page'))), 'error');
    variable_del('bizmappro_is_configured');
    return false;
  } 
  else {
    // clear the 3 cache files.
    file_delete($cache_dir_path . '/' . BIZMAPPRO_CACHE_FILE_BMP_SCRIPT);
    file_delete($cache_dir_path . '/' . BIZMAPPRO_CACHE_FILE_BMP_STYLESHEET);
    file_delete($cache_dir_path . '/' . BIZMAPPRO_CACHE_FILE_GMAP_SCRIPT);
  }

  
  // test that we can download and cache the files
  $error_messages = array();

  $host = $bmp_host . '/script/BizMapProv3.js';
  $filename = $cache_dir_path . '/' . BIZMAPPRO_CACHE_FILE_BMP_SCRIPT;
  if ($res = _bizmappro_get_remote_file($host, $filename)) {
    $error_messages[] = $res;
  }

  $host = $bmp_host . '/css/?Key=' . $subscription_key;
  $filename = $cache_dir_path . '/' . BIZMAPPRO_CACHE_FILE_BMP_STYLESHEET;
  if ($res = _bizmappro_get_remote_file($host, $filename)) {
    $error_messages[] = $res;
  }

  $host = 'http://maps.google.com/maps?file=api&v=2.78&key=' . $gmap_api_key;
  $filename = $cache_dir_path . '/' . BIZMAPPRO_CACHE_FILE_GMAP_SCRIPT;
  if ($res = _bizmappro_get_remote_file($host, $filename)) {
    $error_messages[] = $res;
  }


  if (count($error_messages)) {
    variable_del('bizmappro_is_configured');
    foreach ($error_messages as $message) {
      drupal_set_message($message, 'error');  
    }
    return false;
  }
  else {
    variable_set('bizmappro_is_configured', true);
    drupal_set_message('BizMap Pro has been properly configured.');  
    return true;
  }
}


/**
 * Helper function to download a remote file and save it locally
 */
function _bizmappro_get_remote_file($url, $filename) {
  $req = drupal_http_request($url);
  // Check that our http request gets a valid page (HTTP code 200) and returns data. 
  if ($req->code == 200 && !empty($req->data)) {
    if (0 === file_save_data($req->data, $filename, FILE_EXISTS_REPLACE)) {
	  return t('Error trying to save data to: %filename', array('%filename' => $filename));
    }
  }
  else {
    return t('Error trying to download from URL: %url', array('%url' => $url));
  }
  return false;
}
